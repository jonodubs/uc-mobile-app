Node version: v0.12.7

Cordova version: 5.1.1

Config.xml file: 

<?xml version='1.0' encoding='utf-8'?>
<widget id="com.uc.ucfinder" android-versionCode="13" version="1.2.1" xmlns="http://www.w3.org/ns/widgets" xmlns:gap="http://phonegap.com/ns/1.0">
    <name>UC Finder</name>
    <description>
        UC mobile navigation app.
    </description>
    <author>
        UC App Team
    </author>
    <content src="index.html" />
    <preference name="permissions" value="none" />
    <preference name="phonegap-version" value="3.6.3" />
    <preference name="orientation" value="default" />
    <preference name="target-device" value="universal" />
    <preference name="fullscreen" value="false" />
    <preference name="webviewbounce" value="false" />
    <preference name="DisallowOverscroll" value="true" />
    <preference name="prerendered-icon" value="true" />
    <preference name="stay-in-webview" value="false" />
    <preference name="ios-statusbarstyle" value="lightcontent"/>
    <preference name="StatusBarOverlaysWebView" value="true" />
    <preference name="StatusBarStyle" value="lightcontent" />
    <preference name="detect-data-types" value="true" />
    <preference name="exit-on-suspend" value="false" />
    <preference name="show-splash-screen-spinner" value="true" />
    <preference name="auto-hide-splash-screen" value="true" />
    <preference name="disable-cursor" value="false" />
    <preference name="android-minSdkVersion" value="10" />
    <preference name="android-installLocation" value="auto" />
    <preference name="SplashScreen" value="screen" />
    <preference name="SplashScreenDelay" value="10000" />
    <gap:plugin name="org.apache.cordova.battery-status" version="0.2.11" />
    <gap:plugin name="org.apache.cordova.camera" version="0.3.2" />
    <gap:plugin name="org.apache.cordova.console" version="0.2.11" />
    <gap:plugin name="org.apache.cordova.contacts" version="0.2.13" />
    <gap:plugin name="org.apache.cordova.device" version="0.2.12" />
    <gap:plugin name="org.apache.cordova.device-motion" version="0.2.10" />
    <gap:plugin name="org.apache.cordova.device-orientation" version="0.3.9" />
    <gap:plugin name="org.apache.cordova.dialogs" version="0.2.10" />
    <gap:plugin name="org.apache.cordova.file" version="1.3.1" />
    <gap:plugin name="org.apache.cordova.file-transfer" version="0.4.6" />
    <gap:plugin name="org.apache.cordova.geolocation" version="0.3.10" />
    <gap:plugin name="org.apache.cordova.globalization" version="0.3.1" />
    <gap:plugin name="org.apache.cordova.inappbrowser" version="0.5.4" />
    <gap:plugin name="org.apache.cordova.media" version="0.2.13" />
    <gap:plugin name="org.apache.cordova.media-capture" version="0.3.3" />
    <gap:plugin name="org.apache.cordova.network-information" version="0.2.12" />
    <gap:plugin name="org.apache.cordova.splashscreen" version="0.3.4" />
    <gap:plugin name="org.apache.cordova.vibration" version="0.3.11" />

    <icon src="icon.png" />

    <platform name="android">
        <!-- you can use any density that exists in the Android project -->
        <splash src="www/res/screen/android/screen-hdpi-landscape.png" density="land-hdpi"/>
        <splash src="www/res/screen/android/screen-ldpi-landscape.png" density="land-ldpi"/>
        <splash src="www/res/screen/android/screen-mdpi-landscape.png" density="land-mdpi"/>
        <splash src="www/res/screen/android/screen-xhdpi-landscape.png" density="land-xhdpi"/>
        <splash src="www/res/screen/android/screen-hdpi-portrait.png" density="port-hdpi"/>
        <splash src="www/res/screen/android/screen-ldpi-portrait.png" density="port-ldpi"/>
        <splash src="www/res/screen/android/screen-mdpi-portrait.png" density="port-mdpi"/>
        <splash src="www/res/screen/android/screen-xhdpi-portrait.png" density="port-xhdpi"/>
    </platform>
    <platform name="ios">
        <!-- images are determined by width and height. The following are supported -->
        <splash src="www/res/screen/ios/Default~iphone.png" width="320" height="480"/>
        <splash src="www/res/screen/ios/Default@2x~iphone.png" width="640" height="960"/>
        <splash src="www/res/screen/ios/Default-Portrait~ipad.png" width="768" height="1024"/>
        <splash src="www/res/screen/ios/Default-Portrait@2x~ipad.png" width="1536" height="2048"/>
        <splash src="www/res/screen/ios/Default-Landscape~ipad.png" width="1024" height="768"/>
        <splash src="www/res/screen/ios/Default-Landscape@2x~ipad.png" width="2048" height="1536"/>
        <splash src="www/res/screen/ios/Default-568h@2x~iphone.png" width="640" height="1136"/>
        <splash src="www/res/screen/ios/Default-667h.png" width="750" height="1334"/>
        <splash src="www/res/screen/ios/Default-736h.png" width="1242" height="2208"/>
        <splash src="www/res/screen/ios/Default-Landscape-736h.png" width="2208" height="1242"/>
    </platform>
    <platform name="android">
        <icon src="www/res/icon/android/36.png" density="ldpi" />
        <icon src="www/res/icon/android/48.png" density="mdpi" />
        <icon src="www/res/icon/android/72.png" density="hdpi" />
        <icon src="www/res/icon/android/96.png" density="xhdpi" />
    </platform>
    <platform name="ios">
        <!-- iOS 8.0+ -->
        <!-- iPhone 6 Plus  -->
        <icon src="www/res/icon/ios/icon-60@3x.png" width="180" height="180" />
        <!-- iOS 7.0+ -->
        <!-- iPhone / iPod Touch  -->
        <icon src="www/res/icon/ios/icon-60.png" width="60" height="60" />
        <icon src="www/res/icon/ios/icon-60@2x.png" width="120" height="120" />
        <!-- iPad -->
        <icon src="www/res/icon/ios/icon-76.png" width="76" height="76" />
        <icon src="www/res/icon/ios/icon-76@2x.png" width="152" height="152" />
        <!-- iOS 6.1 -->
        <!-- Spotlight Icon -->
        <icon src="www/res/icon/ios/icon-40.png" width="40" height="40" />
        <icon src="www/res/icon/ios/icon-40@2x.png" width="80" height="80" />
        <!-- iPhone / iPod Touch -->
        <icon src="www/res/icon/ios/icon.png" width="57" height="57" />
        <icon src="www/res/icon/ios/icon@2x.png" width="114" height="114" />
        <!-- iPad -->
        <icon src="www/res/icon/ios/icon-72.png" width="72" height="72" />
        <icon src="www/res/icon/ios/icon-72@2x.png" width="144" height="144" />
        <!-- iPhone Spotlight and Settings Icon -->
        <icon src="www/res/icon/ios/icon-small.png" width="29" height="29" />
        <icon src="www/res/icon/ios/icon-small@2x.png" width="58" height="58" />
        <!-- iPad Spotlight and Settings Icon -->
        <icon src="www/res/icon/ios/icon-50.png" width="50" height="50" />
        <icon src="www/res/icon/ios/icon-50@2x.png" width="100" height="100" />
        <!--iPhone 6+-->
        <icon src="www/res/icon/ios/icon-small@3x.png" width="87" height="87" />
        <icon src="www/res/icon/ios/icon-40@3x.png" width="120" height="120" />
    </platform>

    <icon gap:platform="winphone" src="www/res/icon/windows-phone/icon-48.png" />
    <icon gap:platform="winphone" gap:role="background" src="www/res/icon/windows-phone/icon-173-tile.png" />
    <gap:splash gap:platform="winphone" src="www/res/screen/windows-phone/screen-portrait.jpg" />


    <access origin="*" />
    <access origin="tel:*" launch-external="yes" />
    <access origin="mailto:*" launch-external="yes" />
</widget>


Plugins: 

com.msopentech.websql,org.apache.cordova.device,org.apache.cordova.device-motion,org.apache.cordova.device-orientation,org.apache.cordova.file,org.apache.cordova.file-transfer,org.apache.cordova.geolocation,org.apache.cordova.inappbrowser,org.apache.cordova.network-information,org.apache.cordova.splashscreen,org.apache.cordova.statusbar

Android platform:

Available Android targets:
----------
id: 1 or "android-22"
     Name: Android 5.1.1
     Type: Platform
     API level: 22
     Revision: 2
     Skins: HVGA, QVGA, WQVGA400, WQVGA432, WSVGA, WVGA800 (default), WVGA854, WXGA720, WXGA800, WXGA800-7in, AndroidWearRound, AndroidWearRound400x400, AndroidWearRoundChin320x290, AndroidWearSquare, AndroidWearSquare320x320, AndroidWearRound, AndroidWearRound400x400, AndroidWearRoundChin320x290, AndroidWearSquare, AndroidWearSquare320x320
 Tag/ABIs : android-tv/armeabi-v7a, android-tv/x86, android-wear/armeabi-v7a, android-wear/x86, default/armeabi-v7a, default/x86, default/x86_64
----------
id: 2 or "Google Inc.:Google APIs:22"
     Name: Google APIs
     Type: Add-On
     Vendor: Google Inc.
     Revision: 1
     Description: Android + Google APIs
     Based on Android 5.1.1 (API level 22)
     Libraries:
      * com.android.future.usb.accessory (usb.jar)
          API for USB Accessories
      * com.google.android.media.effects (effects.jar)
          Collection of video effects
      * com.google.android.maps (maps.jar)
          API for Google Maps
     Skins: HVGA, QVGA, WQVGA400, WQVGA432, WSVGA, WVGA800 (default), WVGA854, WXGA720, WXGA800, WXGA800-7in, AndroidWearRound, AndroidWearRound400x400, AndroidWearRoundChin320x290, AndroidWearSquare, AndroidWearSquare320x320, AndroidWearRound, AndroidWearRound400x400, AndroidWearRoundChin320x290, AndroidWearSquare, AndroidWearSquare320x320
 Tag/ABIs : google_apis/x86

