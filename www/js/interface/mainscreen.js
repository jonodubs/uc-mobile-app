/**
 * Created by Robert on 16/12/2014.
 */

function mainDraw(categoryName, items) {
    if (items.length > 0) {
        //Make main item div -> attach to main item container
        var mainItem = document.createElement('DIV');
        mainItem.className = 'main-item highlight-on-click';

        if (inArray(categoryName, mainToMap)) {
            mainItem.addEventListener('click', function() {
                mainMapTransition(items)
            }, false);
        } else {
            mainItem.addEventListener('click', function() {
                mainListTransition(categoryName)
            }, false);
        }

        document.getElementById('main-item-container').appendChild(mainItem);

        //Make main button text div -> attach to main item
        var mainIcon = document.createElement('DIV');
        mainIcon.className = 'main-icon ' + mainPageImages[categoryName];
        mainItem.appendChild(mainIcon);

        //Make main item text div -> attach to main item
        var mainItemText = document.createElement('DIV');
        mainItemText.innerHTML = categoryName;
        mainItemText.className = 'main-item-text';
        mainItem.appendChild(mainItemText);

        var chevron = document.createElement('DIV');
        chevron.className = 'icon-right-open';
        mainItem.appendChild(chevron);

        if (!(inArray(categoryName, mainToMap))) {
            listDraw(categoryName, items);
        }
        highlightAnimation();

        if (categoryName == lastMainItem) {
            navigator.splashscreen.hide();
            //Connectivity checks for non-WP.
            if(android || iOS) {
                document.addEventListener('online', app.onOnline, false);
                document.addEventListener('resume', app.onResume, false);
            }
        }
    }
}

function mainListTransition(categoryName) {
    if (!searchBoxOpen) {
        var screenId = categoryName + '-screen';

        previousScreens.push(mainScreen);
        slideOutLeft(mainScreen);
        slideInRight(screenId);
        currentScreen = screenId;

        if(iOS) {
            document.getElementById('back-icon').style.display = 'block';
            document.getElementById('uc-logo').style.display = 'none';
        } else {
            document.getElementById('uc-logo').setAttribute('src', 'img/university-of-canterbury-coat-of-arms-mobile-back.png');
        }

        document.body.scrollTop = 0;
    }
}

function mainMapTransition(pois) {
    if (!searchBoxOpen) {
        markerDraw(pois);
        userLocation();

        if (userPosition !== null) {
            handleOffCampus();
        } else {
            map.setZoom(poiGroupZoom);
            centerMap();
        }

        previousScreens.push(mainScreen);
        slideOutLeft(mainScreen);
        slideInRight(mapScreen);
        currentScreen = mapScreen;

        if(iOS) {
            document.getElementById('back-icon').style.display = 'block';
            document.getElementById('uc-logo').style.display = 'none';
        } else {
            document.getElementById('uc-logo').setAttribute('src', 'img/university-of-canterbury-coat-of-arms-mobile-back.png');
        }

        overrideLinks();
        google.maps.event.trigger(map, 'resize');
    }
}