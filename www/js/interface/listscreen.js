/**
 * Created by Robert on 16/12/2014.
 */

function listDraw(categoryName, items) {
    //Make list screen div -> attach to body
    var listScreen = document.createElement('DIV');
    listScreen.id = categoryName + '-screen';
    listScreen.className = 'list-screen page transition right';
    document.body.appendChild(listScreen);

    if (iOS) {
        var logoBehind = document.createElement('DIV');
        logoBehind.className = 'uc-logo-behind';
        listScreen.appendChild(logoBehind);
    }

    //Phone number section for emergency page
    if (categoryName == emergency) {
        emergencyDraw();
    }

    for (var i = 0; i < items.length; i++) {
        (function() {
            var item = items[i];
            var title = item[0];
            var pois = item[1];
            var poisLen = pois.length;

            if (poisLen > 0) {
                //Make list item div -> attach to list screen
                var listItem = document.createElement('DIV');
                listItem.id = title + '-item';
                listItem.className = 'list-item';
                listScreen.appendChild(listItem);

                //Make list item text div -> attach to list item
                var listItemText = document.createElement('DIV');
                listItemText.className = 'list-item-text highlight-on-click';
                listItemText.innerHTML = title;
                if (poisLen > 1) {
                    listItemText.onclick = function() {
                        listMapTransition(pois)
                    };
                    if (categoryName == parking) {
                        listItemText.innerHTML += '<div class="subtitle-text">' + pois[0].subtitle + '</div>';
                    }
                } else {
                    listItemText.onclick = function() {
                        listInfoTransition(pois)
                    };
                    listItemText.innerHTML += '<div class="subtitle-text">' + pois[0].subtitle + '</div>';
                }
                listItem.appendChild(listItemText);

                //Make list item button wrapper div -> attach to list item
                var listItemButtonWrapper = document.createElement('DIV');
                listItemButtonWrapper.className = 'icon-wrapper';
                listItemButtonWrapper.onclick = function() {
                    listMapTransition(pois)
                };
                listItem.appendChild(listItemButtonWrapper);

                //Make list item button div -> attach to wrapper item
                var listItemButton = document.createElement('DIV');
                listItemButton.className = 'icon-ion-android-pin scale-on-click';
                listItemButton.onclick = function() {
                    listMapTransition(pois)
                };
                listItem.appendChild(listItemButton);
            }
        })();
    }
    highlightAnimation();
    scaleAnimation();
}

function emergencyDraw() {
    var listScreen = document.getElementById(emergency + '-screen');

    //Make "Emergency Numbers" heading -> attach to list screen
    var numbersHeading = document.createElement('DIV');
    numbersHeading.className = 'phone-number-heading';
    numbersHeading.innerHTML = 'Emergency Numbers';
    listScreen.appendChild(numbersHeading);

    for (var n = 0; n < emergencyNumbers.length; n++) {

        (function() {
            var number = emergencyNumbers[n];

            //Make emergency number item -> attach to list screen
            var numberItem = document.createElement('DIV');
            numberItem.className = 'phone-number-item highlight-on-click';
            numberItem.onclick = function () {
                callNumber(number[2]);
            };
            listScreen.appendChild(numberItem);

            //Make emergency number item text -> attach to emergency number item
            var numberItemText = document.createElement('DIV');
            numberItemText.className = 'list-item-text';
            numberItemText.innerHTML = number[0];
            numberItem.appendChild(numberItemText);

            //Make emergency number item button -> attach to emergency number item
            var numberItemButton = document.createElement('DIV');
            numberItemButton.className = 'icon-phone';
            numberItem.appendChild(numberItemButton);

            var numberText = document.createElement('DIV');
            numberText.className = 'number-text';
            numberText.innerHTML = number[1];
            numberItemText.appendChild(numberText);
        })();
    }

    //Make "Important Locations" heading -> attach to list screen
    var itemsHeading = document.createElement('DIV');
    itemsHeading.className = 'phone-number-heading phone-border';
    itemsHeading.innerHTML = 'Important Locations';
    listScreen.appendChild(itemsHeading);
}

function listMapTransition(pois) {
    if (!searchBoxOpen) {
        markerDraw(pois);
        userLocation();
        if (userPosition === null) {
            centerMap();
        } else {
            if (pois.length == 1) {
                followUser = false;
                var poiLocation = new google.maps.LatLng(pois[0].lat, pois[0].lng);

                if (userPosition !== null) {
                    setBounds(poiLocation);
                }
            } else {
                handleOffCampus();
            }
        }

        previousScroll = document.body.scrollTop;
        previousScreens.push(currentScreen);
        slideOutLeft(currentScreen);
        slideInRight(mapScreen);
        currentScreen = mapScreen;

        overrideLinks();
        google.maps.event.trigger(map, 'resize');
    }
}

function listInfoTransition(poi) {
    if (!searchBoxOpen) {
        infoDraw(poi);

        previousScroll = document.body.scrollTop;
        previousScreens.push(currentScreen);
        slideOutLeft(currentScreen);
        slideInRight(infoScreen);
        currentScreen = infoScreen;
    }
}

function callNumber(number) {
    window.location = number;
}

function sortListScreen(screenId) {
    var screen = document.getElementById(screenId);

    [].map.call(screen.children, Object).sort(function (a, b) {
        return a.id > b.id;
    }).forEach(function (elem) {
        screen.appendChild(elem);
    });
}