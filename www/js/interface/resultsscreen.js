/**
 * Created by Robert on 22/01/2015.
 */

function resultsDraw(pois) {
    if(pois !== null && pois.length > 0) {
        for (var p = 0; p < pois.length; p++) {
            (function() {
                var poi = pois[p];

                //Make results item div -> attach to results screen
                var resultsItem = document.createElement('DIV');
                var resultsScreenDiv = document.getElementById(resultsScreen);
                resultsItem.className = 'list-item';
                resultsScreenDiv.appendChild(resultsItem);

                //Make results item text div -> attach to results item
                var resultsItemText = document.createElement('DIV');
                resultsItemText.className = 'list-item-text highlight-on-click';
                resultsItemText.innerHTML = poi.title;
                resultsItemText.innerHTML += '<div class="subtitle-text">' + poi.subtitle + '</div>';
                resultsItemText.onclick = function() {
                    resultsInfoTransition([poi])
                };
                resultsItem.appendChild(resultsItemText);

                //Make results item button wrapper div -> attach to results item
                var resultsItemButtonWrapper = document.createElement('DIV');
                resultsItemButtonWrapper.className = 'icon-wrapper';
                resultsItemButtonWrapper.onclick = function() {
                    resultsMapTransition([poi])
                };
                resultsItem.appendChild(resultsItemButtonWrapper);

                //Make results item button div -> attach to wrapper item
                var resultsItemButton = document.createElement('DIV');
                resultsItemButton.className = 'icon-ion-android-pin scale-on-click';
                resultsItemButton.onclick = function() {
                    resultsMapTransition([poi])
                };
                resultsItem.appendChild(resultsItemButton);
            })();
        }
    } else {
        var resultsItem = document.createElement('DIV');
        var resultsScreenDiv = document.getElementById(resultsScreen);
        resultsItem.className = 'list-item-text';
        resultsItem.innerHTML="No results found";
        resultsScreenDiv.appendChild(resultsItem);
    }
}

function resultsMapTransition(poi) {
    if (!searchBoxOpen) {
        removeFromArray(mapScreen, previousScreens);

        markerDraw(poi);
        userLocation();
        followUser = false;
        var poiLocation = new google.maps.LatLng(poi[0].lat, poi[0].lng);

        if (userPosition !== null) {
            setBounds(poiLocation);
        } else {
            centerMap();
        }

        previousScroll = document.body.scrollTop;
        previousScreens.push(resultsScreen);
        slideOutLeft(resultsScreen);
        slideInRight(mapScreen);
        currentScreen = mapScreen;

        overrideLinks();
        google.maps.event.trigger(map, 'resize');
    }
}

function resultsInfoTransition(poi) {
    if (!searchBoxOpen) {
        infoDraw(poi);
        removeFromArray(infoScreen, previousScreens);

        previousScroll = document.body.scrollTop;
        previousScreens.push(resultsScreen);
        slideOutLeft(resultsScreen);
        slideInRight(infoScreen);
        currentScreen = infoScreen;
    }
}

