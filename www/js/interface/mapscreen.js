/**
 * Created by Robert on 11/12/2014.
 */

var map = null;
var defaultZoom = 14;
var defaultLat = -43.524555;
var defaultLng = 172.577227;
var ucCenter = null;

var markers = [];

var userPosition = null;
var userPositionMarker = null;
var userPositionMarkerImg = null;
var userDirection = null;
var userDirectionMarker = null;
var userDirectionMarkerImg = null;
var infoWindow = null;

var followUser = true;

var posTracker = null;
var dirTracker = null;

//Map styling.
var mapStyles = [
    {
        "featureType": "road",
        "elementType": "geometry",
        "stylers": [
            {"color": "#ffffff"}
        ]
    }, {
        "featureType": "water",
        "elementType": "geometry",
        "stylers": [
            //{"color": "#50b8fa"}
            {"color": "#abd6fa"}
        ]
    }, {
        "elementType": "labels.text.fill",
        "stylers": [
            {"color": "#111111"}
        ]
    }, {
        "featureType": "administrative.land_parcel",
        "stylers": [
            {"visibility": "off"}
        ]
    }, {
        "featureType": "poi.park",
        "elementType": "geometry",
        "stylers": [
            {"visibility": "simplified"},
            //{"color": "#A1B70D"}
            {"color": "#a7b76c"}
        ]
    }, {
        "featureType": "landscape.man_made",
        "elementType": "geometry.fill",
        "stylers": [
            {"visibility": "on"},
            {"color": "#dddddd"}
        ]
    }, {
        "featureType": "landscape.natural",
        "elementType": "geometry",
        "stylers": [
            {"visibility": "off"}
        ]
    }, {
        "featureType": "landscape.man_made",
        "elementType": "geometry.stroke",
        "stylers": [
            {"color": "#aaaaaa"},
            {"weight": 1.1}
        ]
    }, {
        "featureType": "poi.school",
        "elementType": "geometry.fill",
        "stylers": [
            {"color": "#eeeeee"}
        ]
    }, {
        "featureType": "poi.sports_complex",
        "elementType": "geometry.fill",
        "stylers": [
            //{"color": "#A1B70D"}
            {"color": "#a7b76c"}
        ]
    }, {
        "featureType": "water"}
];

//Map directions drawing variables
var selectedMarker = null;
var selectedMode = 'WALKING';
var directionsOptions = {
    map: null,
    suppressMarkers: true,
    preserveViewport: false,
    polylineOptions: {
        strokeColor: '#2173e4',
        strokeOpacity: 0.5,
        strokeWeight: 5
    }
};

var iosDirectionsOptions = {
    map: null,
    suppressMarkers: true,
    preserveViewport: false,
    polylineOptions: {
        strokeColor: '#2173e4',
        strokeOpacity: 0.75,
        strokeWeight: 5
    }
};
var directionsDisplay = null;
var directionsService = null;

var overlay;
var overlaySwLat = -43.528891;
var overlaySwLng = 172.564150;
var overlayNeLat = -43.518789;
var overlayNeLng = 172.590243;
var overlayImg = 'img/uc-overlay.png';

//Draws map using mapOptions.
function mapDraw() {
    ucCenter = new google.maps.LatLng(defaultLat, defaultLng);
    userPositionMarkerImg = {
        url: 'img/user-location.gif',
        origin: new google.maps.Point(0, 0),
        anchor: new google.maps.Point(21.5, 21.5),
        scaledSize: new google.maps.Size(43, 43)
    };
    userDirectionMarkerImg = {
        path: google.maps.SymbolPath.FORWARD_CLOSED_ARROW,
        fillColor: '#2173E4',
        fillOpacity: 1,
        strokeWeight: 0,
        origin: new google.maps.Point(0, 0),
        anchor: new google.maps.Point(0, 5),
        scale: 4
    };

    //Initial map options.
    var mapOptions = {
        center: ucCenter,
        zoom: defaultZoom,
        styles: mapStyles,
        disableDefaultUI: true
    };

    map = new google.maps.Map(mapCanvas, mapOptions);

    var swBound = new google.maps.LatLng(overlaySwLat, overlaySwLng);
    var neBound = new google.maps.LatLng(overlayNeLat, overlayNeLng);
    var bounds = new google.maps.LatLngBounds(swBound, neBound);

    overlay = new google.maps.GroundOverlay(overlayImg, bounds);
    overlay.setMap(map);

    directionsService = new google.maps.DirectionsService();
    directionsOptions.map = map;

    userLocation();

    document.getElementById('map-center').addEventListener('touchstart', function() {
        centerMap();
    }, false);

    document.getElementById(mapScreen).addEventListener('touchmove', function() {
        followUser = false
    }, false);

    document.getElementById(mapScreen).addEventListener('touchstart', function() {
        if (userPosition === null) {
            userLocation();
        }
    }, false);

    document.getElementById('directions-choice-wrapper').addEventListener('click', function() {
        document.getElementById('directions-choice-wrapper').style.display = 'none';
    }, false);

    window.addEventListener("resize", function() {
        if (currentScreen == mapScreen) {
            mapHeight = document.documentElement.clientHeight - headerHeight;
            mapCanvas.style.height = mapHeight + 'px';
            google.maps.event.trigger(map, 'resize');
        }
    }, false);

    var directionChoices = document.getElementsByClassName('directions-choice');
    for (var dc = 0; dc < directionChoices.length; dc++) {
        directionChoices[dc].addEventListener('click', function() {
            if (navigator.connection.type != Connection.NONE) {
                infoWindow.close();
                document.getElementById('directions-choice-wrapper').style.display = 'none';
                selectedMode = this.id;
                drawRoute(selectedMode, selectedMarker.position, false)
            } else {
                alert('Sorry, the Directions feature requires an internet connection');
            }
        }, false);
    }

    document.addEventListener('pause', function () {
        navigator.geolocation.clearWatch(posTracker);
        posTracker = null;
    }, false);

    document.addEventListener('resume', function () {
        userLocation();
    }, false);

    overrideLinks();

    mapCanvas.style.webkitTransform = 'translateZ(0px)';
}

//Takes array of poiModels and creates + draws markers.
function markerDraw(pois) {
    if (navigator.connection.type == Connection.NONE) {
        if (!map) {
            alert('Sorry, the maps feature requires internet access');
        } else {
            alert('Sorry, map imagery may be missing without internet access');
        }
    }

    if(directionsDisplay != null) {
        directionsDisplay.setMap(null);
        directionsDisplay = null;
        selectedMode = 'WALKING';
    }

    markers.forEach(function(m) {
        m.setMap(null);
    });
    markers.length = 0;
    pois.forEach(function(p) {
        var imageMarker = {
            url: categoryImages[p.category],
            scaledSize: new google.maps.Size(40, 40)
        };
        var marker = new google.maps.Marker({
            position: new google.maps.LatLng(p.lat, p.lng),
            title: p.title,
            icon: imageMarker,
            map: map
        });

        if (infoWindow === null) {
            infoWindow = new google.maps.InfoWindow({
                noSuppress: true
            });
        }

        google.maps.event.addListener(marker, 'click', function() {
            var infoDiv = document.createElement('div');
            infoDiv.className = 'map-info';

            var infoTextDiv = document.createElement('div');
            infoTextDiv.innerHTML = p.title;
            infoDiv.appendChild(infoTextDiv);

            var infoDirectionsDiv = document.createElement('div');
            infoDirectionsDiv.className = 'icon-direction-arrow map-button';
            infoDirectionsDiv.addEventListener('click', function () {
                document.getElementById('directions-choice-wrapper').style.display = 'block';
                selectedMarker = marker;
            }, false);
            infoDiv.appendChild(infoDirectionsDiv);

            var infoInfoDiv = document.createElement('div');
            infoInfoDiv.className = 'icon-info map-button';
            infoInfoDiv.addEventListener('click', function () {
                mapInfoTransition([p])
            }, false);
            infoDiv.appendChild(infoInfoDiv);

            infoWindow.setContent(infoDiv);
            infoWindow.open(map, marker)
        });

        markers.push(marker);
    });

    if (markers.length == 1) {
        selectedMarker = markers[0];
        drawRoute('WALKING', markers[0].position, true)
    }

    if (navigator.compass && dirTracker === null) {
        dirTracker = navigator.compass.watchHeading(function (heading) {
            if (heading) {
                userDirection = heading.magneticHeading;
            }
            userDirectionMarkerImg.rotation = userDirection;
            userDirectionMarker.setIcon(userDirectionMarkerImg);
        }, null, {frequency: 1000});
    }
}

function userMarkerDraw(position) {
    userPosition = new google.maps.LatLng(position.coords.latitude, position.coords.longitude);

    if (userPositionMarker === null) {
        userPositionMarker = new google.maps.Marker({
            position: userPosition,
            map: map,
            icon: userPositionMarkerImg,
            optimized: false,
            zIndex: 1
        });
    } else {
        userPositionMarker.setPosition(userPosition);
        if (userDirectionMarker) {
            userDirectionMarker.setPosition(userPosition);
        }
        if (followUser === true) {
            centerMap();
        }
        if(directionsDisplay != null && navigator.connection.type != Connection.NONE) {
            drawRoute(selectedMode, selectedMarker.position, true)
        }
    }

    if (userDirectionMarker === null) {
        userDirectionMarker = new google.maps.Marker({
            position: userPosition,
            icon: userDirectionMarkerImg,
            map: map,
            zIndex: 2
        });
    }
}

//Handles current user location.
function userLocation() {
    if(navigator.geolocation && !posTracker) {
        posTracker = navigator.geolocation.watchPosition(function(position) {
            userMarkerDraw(position);
        }, function() {
            if (!iOS) {
                userPosition = null;
            }
        }, {maximumAge: Infinity, timeout: 5000, enableHighAccuracy: true});
    } else {
    }
}

function setBounds(location) {
    var bounds = new google.maps.LatLngBounds();
    bounds.extend(location);
    bounds.extend(userPosition);
    map.fitBounds(bounds);
}

function centerMap() {
    if (userPosition !== null) {
        followUser = true;
        map.panTo(userPosition);
    } else {
        alert('Sorry, we cannot find your current location. GPS may be disabled on your phone.');
        map.setZoom(defaultZoom);
        map.panTo(ucCenter);
    }
}

function overrideLinks() {
    var links = document.getElementById(mapScreen).getElementsByTagName('a');
    var len = links.length;
    var termsDomain = 'http://www.google.com';

    for (var i = 0; i < len; i++) {
        var link = links[i];
        var uri = link.href;
        if (uri.slice(0, termsDomain.length) == termsDomain) {
            link.addEventListener('click', function(e){
                e.preventDefault();
                var browserWindow = window.open(this.href, '_system', 'location=yes');
                browserWindow.addEventListener("exit", function() {
                    browserWindow.close();
                });
            }, false);
        } else if (uri) {
            link.addEventListener('click', function(e){
                e.preventDefault();
            }, false);
        }
    }

    var set = google.maps.InfoWindow.prototype.set;

    google.maps.InfoWindow.prototype.set = function (key, val) {
        if (key === 'map' && ! this.get('noSuppress')) {
            return;
        }

        set.apply(this, arguments);
    }
}

function distance(lat1, lng1, lat2, lng2) {
    var R = 6371;
    var a =
        0.5 - Math.cos((lat2 - lat1) * Math.PI / 180)/2 +
        Math.cos(lat1 * Math.PI / 180) * Math.cos(lat2 * Math.PI / 180) *
        (1 - Math.cos((lng2 - lng1) * Math.PI / 180))/2;

    return (R * 2 * Math.asin(Math.sqrt(a))) * 1000;
}

function handleOffCampus() {
    var onCampus = false;
    var closestUniPoint = null;
    var shortestDistance = 21000000;
    var numberOfUniPoints = uniPoints.length;

    for (var p = 0; p < numberOfUniPoints; p++) {
        var point = uniPoints[p];
        var dist = distance(userPosition.lat(), userPosition.lng(), point.lat, point.lng);
        if (dist < uniPointRadius) {
            onCampus = true;
            break;
        } else if (dist < shortestDistance) {
            shortestDistance = dist;
            closestUniPoint = point;
        }
    }

    if (onCampus) {
        map.setZoom(poiGroupZoom);
        centerMap();
    } else {
        var closestLatLng = new google.maps.LatLng(closestUniPoint.lat, closestUniPoint.lng);
        setBounds(closestLatLng);
    }
}

function drawRoute(mode, destination, isUpdate) {
    directionsOptions.preserveViewport = isUpdate;

    var directionsRequest = {
        origin: userPosition,
        destination: destination,
        travelMode: google.maps.TravelMode[mode]
    };

    directionsService.route(directionsRequest, function(response, status) {
        if (status == google.maps.DirectionsStatus.OK) {
            if(directionsDisplay != null) {
                directionsDisplay.setMap(null);
                directionsDisplay = null;
            }
            if (iOS) {
                directionsDisplay = new google.maps.DirectionsRenderer(iosDirectionsOptions);
            } else {
                directionsDisplay = new google.maps.DirectionsRenderer(directionsOptions);
            }
            directionsDisplay.setMap(map);
            directionsDisplay.setDirections(response);
            google.maps.event.trigger(map, 'resize');
        }
    });
}

function mapInfoTransition(poi){
    if (!searchBoxOpen) {
        infoDraw(poi);
        removeFromArray(infoScreen, previousScreens);

        previousScreens.push(mapScreen);
        slideOutLeft(mapScreen);
        slideInRight(infoScreen);
        currentScreen = infoScreen;
    } else {
        searchClose();
    }
}

