/**
 * Created by rjo72 on 12/19/2014.
 */

//Local database.
var myDB;
//Pre DB insertion container for POI objects.
var poiArray = [];
//Categories of POI that do not use the "landing screen -> List of items -> map (single POI)" structure.
var parkingArray = [];
var busBikeArray = [];
var emergencyArray = [];
var servicesArray = [];
var foodArray = [];
var spacesArray = [];
var accessArray = [];
//Counters used to ensure all POIs in select menu items have been pulled from the DB before building menu items.
var parkingSelected = 0;
var busBikeSelected = 0;
var emergencySelected = 0;
var servicesSelected = 0;
var foodSelected = 0;
var spacesSelected = 0;
var accessSelected = 0;

function initializeDB() {
    myDB = window.openDatabase("ucmobdb", "1.0", "uc mobile db", 1000000);
    //Get timestamp of latest POI database update.
    var serverTimestamp = requestTimestamp();
    //The time that the POI data on the device was last updated.
    var clientTimestamp = window.localStorage.getItem('timestamp');
    //The last update that was installed and forced a POI database update.
    var lastAppVersion = window.localStorage.getItem('lastAppVersion');

    //If POI has not been updated
    if (serverTimestamp == clientTimestamp && appVersion == lastAppVersion) {
        //Construct menus from locally stored data.
        insertSuccess();
    } else {
        //Update data in the local DB.
        window.localStorage.setItem('lastAppVersion', appVersion);
        window.localStorage.setItem('timestamp', serverTimestamp);
        myDB.transaction(createTable, tableError, getAllPOIs);
    }
}

//Issue GET request for the time of the most recent server database update.
function requestTimestamp() {
    var timestamp = window.localStorage.getItem('timestamp');
    if (navigator.connection.type != Connection.NONE) {
        var uri = 'http://www.canterbury.ac.nz/maps/lastupdated';
        var xhr = new XMLHttpRequest();
        xhr.open('GET', uri, false);
        xhr.onload = function() {
            timestamp = this.responseText;
        };
        xhr.send(null);
        return timestamp;
    }
    return timestamp;
}

//Empty/create DB table and create indices to aid later SELECT queries.
function createTable(tx) {
    tx.executeSql('DROP TABLE IF EXISTS poi_table');
    tx.executeSql('CREATE TABLE IF NOT EXISTS poi_table (' +
    '"id" INTEGER PRIMARY KEY NOT NULL UNIQUE, ' +
    '"title" TEXT NOT NULL, ' +
    '"description" TEXT NOT NULL, ' +
    '"category" INTEGER NOT NULL, ' +
    '"icon" TEXT NOT NULL, ' +
    '"image" TEXT, ' +
    '"lat" TEXT NOT NULL, ' +
    '"lng" TEXT NOT NULL, ' +
    '"subtitle" TEXT)');
    tx.executeSql('CREATE INDEX cat ON poi_table (category)');
    tx.executeSql('CREATE INDEX title ON poi_table (title)');
}

//Handle table creation/emptying errors.
function tableError(error) {
    alert('Table Creation Error: ' + error.message);
}

//Retrieve POI data from CMS server.
function getAllPOIs() {
    //Categories to fetch using POST request.
    var categoriesSelected = [];

    //Convert category names to id numbers and format for POST request.
    for (var c = 0; c < categoryNames.length; c++) {
        var categoryID = nameToId[categoryNames[c]];
        categoriesSelected.push(JSON.stringify(categoryID));
    }

    //Send POST request to CMS.
    var xhr = new XMLHttpRequest();
    var categoryUri = 'http://www.canterbury.ac.nz/maps/browse';
    xhr.open('POST', categoryUri, false); //TODO: Fix asycn issue.
    xhr.setRequestHeader('Content-type', 'application/x-www-form-urlencoded');
    xhr.onload = function() {
        //Parse JSON response into POIModels.
        createPOIModels(JSON.parse(this.responseText));
    };
    xhr.send('data=' + JSON.stringify(categoriesSelected));

    //Retrieve individual POIs that fall outside of already retrieved categories.
    for (var i = 0; i < individualIds.length; i++) {
        var id = individualIds[i];
        var individualUri = 'http://www.canterbury.ac.nz/maps/poi';
        var xhri = new XMLHttpRequest();
        xhri.open('POST', individualUri, false); //TODO: Fix asycn issue.
        xhri.setRequestHeader('Content-type', 'application/x-www-form-urlencoded');
        xhri.onload = function() {
            //Parse JSON response into a POI object.
            createPOIModels(JSON.parse(this.responseText));
        };
        xhri.send('data=' + id);
    }

    //Insert POIs into DB.
    fillDB();
}

//Convert JSON response into POI objects.
function createPOIModels(pois) {
    pois.forEach(function(p) {
        //"cat_id" may be a subcategory that we wish to merge with other subcategories before using.
        var category;
        var poiCategory = p["cat_id"];

        //Merge lecture/department "cat_id" subcategories into single lecture/department categories.
        if (inArray(poiCategory, lectureSubCategories)) {
            category = lectureCategory;
        } else if (inArray(poiCategory, departmentSubCategories)) {
            category = departmentCategory;
        } else {
            //If not a member of a subcategory.
            category = poiCategory;
        }

        //Prepare POI data for insertion into DB.
        var poi = new POIModel(category, p.description, p.icon, p.id, p.image, p.lat, p.lng, p.subtitle, p.title);
        poiArray.push(poi);
    });
}

function fillDB() {
    myDB.transaction(insertPOIs, insertError, insertSuccess);
}

//Insert POI objects into the database.
function insertPOIs(tx) {
    poiArray.forEach(function(p) {
        //Replace quotation marks in description text to prevent SQL query bug.
        var desc = p.description;
        var subtitle = p.subtitle;
        var pattern = /(["])+/g;
        //Build insertion query.
        var query = 'INSERT INTO poi_table (id, title, description, category, icon, image, lat, lng, '
            + 'subtitle) VALUES ('
            + ' "' + p.id + '",'
            + ' "' + removeDiacritics(p.title) + '",'
            + ' "' + desc.replace(pattern, '') + '",'
            + ' "' + p.category + '",'
            + ' "' + p.icon + '",'
            + ' "' + p.image + '",'
            + ' "' + p.lat + '",'
            + ' "' + p.lng + '",'
            + ' "' + p.subtitle + '"'
            + ')';
        tx.executeSql(query);
    });
}

//todo optimize pulling data after insertion
//Pull POI objects from the DB for building menus and map markers.
function insertSuccess() {
    //Pull POIs that are included in a category.
    var numberOfCategories = categoryNames.length;

    //Select POIs using category values and a callback function that processes the results.
    for (var c = 0; c < numberOfCategories; c++) {
        var categoryName = categoryNames[c];
        var categoryID = nameToId[categoryName];
        selectByCategory(categoryID, selectCategorySuccess);
    }

    //Pull individual POIs that are not included in any category.
    var numberOfIndividuals = individualIds.length;

    //Select POIs using individual IDs and a callback function that processes the results.
    for (var i = 0; i < numberOfIndividuals; i++) {
        var id = individualIds[i];
        selectById(id, selectIdSuccess);
    }
}

//Handle table insertion errors.
function insertError(error) {
    alert('Insertion Error: ' + error.message);
}
//Select POI objects by category
function selectByCategory(categoryID, callback) {
    var query = 'SELECT * FROM poi_table WHERE category=' + categoryID + ' ORDER BY title';
    myDB.transaction(function(tx) {
        tx.executeSql(query, [], function(tx, rs) {
            callback(rs, categoryID);
        }, selectError)
    });
}

//Function passed as a callback when selecting by category.
function selectCategorySuccess(results, categoryID) {
    var len = results.rows.length;
    var pois = [];

    //Process all POIs selected
    for (var i = 0; i < len; i++) {
        var temp = results.rows.item(i);
        var poi = new POIModel(temp.category, temp.description, temp.icon, temp.id, temp.image, temp.lat, temp.lng,
            temp.subtitle, temp.title);
        ///If POI will be included in "landing screen -> List of items -> map (single POI)" screen structure.
        if (inArray(categoryID, singlePoiListCategories) || categoryID == establishments) {
            //Structure = [innerHTML for menu item, [POI Object(s) for markerDraw() function]]
            pois.push([poi.title, [poi]]);
        } else {
            //innerHTML values will be added later.
            pois.push(poi);
        }
    }

    //If POIs selected are from a "landing screen -> List of items -> map (single POI)" category, then create menu item.
    if (inArray(categoryID, singlePoiListCategories)) {
        mainDraw(idToName[categoryID], pois);
    } else if (inArray(categoryID, parkingCategories)) {
        //Group parking subcategories.
        parkingArray.push([idToName[categoryID], pois]);
        parkingSelected += 1;
        //Create menu item once all parking subcategories have been processed.
        if (parkingSelected == parkingCategories.length) {
            mainDraw(parking, parkingArray);
        }
    } else if (inArray(categoryID, accessCategories)) {
        //Group accessibility subcategories.
        accessArray.push([idToName[categoryID], pois]);
        accessSelected += 1;
        //Create menu item once all accessibility subcategories have been processed.
        if (accessSelected == accessCategories.length) {
            mainDraw(access, accessArray);
        }
    } else if (inArray(categoryID, busBikeCategories)) {
        //Merge bus and bike subcategories.
        busBikeArray = busBikeArray.concat(pois);
        busBikeSelected += 1;
        //Create menu item once all bus and bike POI objects have been processed.
        if (busBikeSelected == busBikeCategories.length) {
            mainDraw(busBike, busBikeArray);
        }
    } else if (inArray(categoryID, spacesCategories)) {
        //Merge student and study spaces subcategories.
        spacesArray = spacesArray.concat(pois);
        spacesSelected += 1;
        //Create menu item once all student spaces POI objects have been processed.
        if (spacesSelected == spacesCategories.length) {
            mainDraw(spaces, spacesArray);
        }
    } else if (inArray(categoryID, emergencyCategories)) {
        //Group emergency screen subcategories.
        emergencyArray.push([idToName[categoryID], pois]);
        emergencySelected += 1;
        //Create menu item once all emergency subcategories have been processed.
        if (emergencySelected == emergencyCategories.length) {
            mainDraw(emergency, emergencyArray);
        }
    } else if (inArray(categoryID, servicesCategories)) {
        //Group service screen subcategories.
        servicesArray.push([idToName[categoryID], pois]);
        servicesSelected += 1;
        //Create menu item once all service subcategories have been processed.
        if (servicesSelected == servicesCategories.length) {
            mainDraw(services, servicesArray);
        }
    } else if (inArray(categoryID, foodCategories)) {
        //Group food and drink screen subcategories.
        if (categoryID == establishments) {
            foodArray = foodArray.concat(pois);
        } else {
            foodArray = foodArray.concat([[idToName[categoryID], pois]]);
        }
        foodSelected += 1;
        //Create menu item once all food and drink subcategories have been processed.
        if (foodSelected == foodCategories.length) {
            mainDraw(food, foodArray);
        }
    }
}

//Select POI objects by ID.
function selectById(id, callback) {
    var query = 'SELECT * FROM poi_table WHERE id=' + id;
    myDB.transaction(function(tx) {
        tx.executeSql(query, [], function(tx, rs) {
            callback(rs);
        }, selectError)
    });
}

//Function passed as a callback when selecting by ID.
function selectIdSuccess(results) {
    //POI will be attached to services screen.
    var screenId = services + '-screen';
    var temp = results.rows.item(0);
    var poi = new POIModel(temp.category, temp.description, temp.icon, temp.id, temp.image, temp.lat, temp.lng,
        temp.subtitle, temp.title);

    //Make list item div -> attach to services screen
    var listScreen = document.getElementById(screenId);
    var listItem = document.createElement('DIV');
    listItem.id = poi.title + '-item';
    listItem.className = 'list-item';
    listScreen.appendChild(listItem);

    //Make list item text div -> attach to list item
    var listItemText = document.createElement('DIV');
    listItemText.className = 'list-item-text highlight-on-click';
    listItemText.innerHTML = poi.title;
    //Put POI object in array for later iteration.
    listItemText.onclick = function() {
        listInfoTransition([poi])
    };
    listItem.appendChild(listItemText);

    //Make list item button wrapper div -> attach to list item
    var listItemButtonWrapper = document.createElement('DIV');
    listItemButtonWrapper.className = 'icon-wrapper';
    //Put POI object in array for later iteration.
    listItemButtonWrapper.onclick = function() {
        listMapTransition([poi])
    };
    listItem.appendChild(listItemButtonWrapper);

    //Make list item button div -> attach to wrapper item
    var listItemButton = document.createElement('DIV');
    listItemButton.className = 'icon-ion-android-pin scale-on-click';
    //Put POI object in array for later iteration.
    listItemButton.onclick = function() {
        listMapTransition([poi])
    };
    listItem.appendChild(listItemButton);

    //Once all individual items attached to services screen, sort '-item' divs by div id.
    if (poi.id == individualIds[individualIds.length - 1]) {
        sortListScreen(screenId)
    }
}

//Handle POI selection errors.
function selectError(error) {
    alert('Selection Error: ' + error.message);
}

//Select POI objects from the DB given a string to search titles for.
function searchPOI(searchKey, callback) {
    var query = 'SELECT * FROM poi_table WHERE title LIKE "%' + searchKey + '%" OR subtitle LIKE "%'+searchKey+'%" ORDER BY title';
    myDB.transaction(function(tx) {
        tx.executeSql(query, [], function(tx, rs) {
            callback(rs);
        }, searchError)
    });
}

//Handle DB search errors.
function searchError(error) {
    alert('Search Error: ' + error.message);
}


//Function passed a callback to handle search results.
function searchSuccess(results) {
    var len = results.rows.length;
    var pois = [];

    //Create POI objects from the selection results.
    for (var i = 0; i < len; i++) {
        var temp = results.rows.item(i);
        var poi = new POIModel(temp.category, temp.description, temp.icon, temp.id, temp.image, temp.lat, temp.lng,
            temp.subtitle, temp.title);
        pois.push(poi);
    }

    //Create a list view from the search result POI items.
    resultsDraw(pois);
}




