/**
 * Created by rjo72 on 12/10/2014.
 */

//Used to construct instances of POI objects. These objects contain the data used to create Google maps markers.
function POIModel(category, description, icon, id, image, lat, lng, subtitle, title){
    this.category = category;
    this.description = description;
    this.icon = icon;
    this.id = id;
    this.image = image;
    this.lat = lat;
    this.lng = lng;
    this.subtitle = subtitle;
    this.title = title;
}




