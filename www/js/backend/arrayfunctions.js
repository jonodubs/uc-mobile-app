/**
 * Created by Robert on 9/02/2015.
 */

//Checks if an item is in a given array.
function inArray(item, array) {
    return (array.indexOf(item) > -1);
}

//Remove first occurrence of item from an array
function removeFromArray (item, array) {
    if (inArray(item, array)) {
        var index = array.indexOf(item);
        array.splice(index, 1);
    }
}